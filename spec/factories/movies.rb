FactoryBot.define do
  factory :movie do
    title { Faker::Movie.title }
    release_year { "2020-10-10" }
  end
end
