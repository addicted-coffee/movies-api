require 'rails_helper'

RSpec.describe "Healths", type: :request do
  before { get '/_health' }

  specify { expect(response).to have_http_status(:ok) }
end
