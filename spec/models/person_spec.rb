# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Person, type: :model do
  describe 'attributes' do
    subject(:person) { create(:person) }

    it 'has correct attributes' do
      is_expected.to respond_to(:first_name)
      is_expected.to respond_to(:last_name)
      is_expected.to respond_to(:aliases)
    end
  end
end
