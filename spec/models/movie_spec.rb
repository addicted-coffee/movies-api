# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe 'attributes' do
    subject(:movie) { create(:movie) }

    it 'has correct attributes' do
      is_expected.to respond_to(:title)
      is_expected.to respond_to(:release_year)
    end
  end
end
